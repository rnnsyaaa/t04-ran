import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Program Pembagian Bilangan");

        boolean validInput = false;
        do {
            try{
                System.out.print("Masukkan Bilangan Pembilang : ");
                int pembilang = scanner.nextInt();
                System.out.print("Masukkan Bilangan Penyebut : ");
                int penyebut = scanner.nextInt();
                int hasil = pembagian(pembilang,  penyebut);
                System.out.println("Hasil = "+hasil);
                validInput =true;
            }
            catch (InputMismatchException e){
                System.out.println("Masukkan Bilangan Bulat ");
                scanner.nextLine();
            }
            catch (ArithmeticException e){
                System.out.println(" Penyebut Tidak Boleh Bernilai = 0");
                scanner.nextLine();
            }
        } while (!validInput);

        scanner.close();
    }

    public static int pembagian(int pembilang, int penyebut) {
        //add exception apabila penyebut bernilai 0
        if (penyebut==0){
            throw new ArithmeticException("Penyebut Tidak Boleh Bernilai 0");
        }
        return pembilang / penyebut;
    }
}
